/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Camellos5;

public class Pantalla extends Thread {

    private Cursa curs;

    public Pantalla(Cursa curs) {
        this.curs = curs;
    }

    public void mostrar() {
        String asterJoseLuis = generaNaster(curs.getCamells(1));
        String asterMariCarmen = generaNaster(curs.getCamells(2));
        String asterAbelardo = generaNaster(curs.getCamells(3));
        String asterBonifacio = generaNaster(curs.getCamells(4));

        System.out.println("\n\nCARRERA DE CAMELLOS\n\n\nCamello Jose Luis:   " + asterJoseLuis + "\n");
        System.out.println("Camello Mari Carmen: " + asterMariCarmen + "\n");
        System.out.println("Camello Abelardo:    " + asterAbelardo + "\n");
        System.out.println("Camello Bonifacio:   " + asterBonifacio + "\n");
    }

    String generaNaster(int n) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {

            sb.append("*");
        }
        return sb.toString();
    }

    @Override
    public void run() {
        while (curs.getGuanyador() == -1) {
            mostrar();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
        
        if (curs.getGuanyador() == 1) {
            System.out.println("El camello ganador es Jose Luis");
        } else if (curs.getGuanyador() == 2) {       
            System.out.println("El camello ganador es Mari Carmen");
        } else if (curs.getGuanyador() == 3) {          
            System.out.println("El camello ganador es Abelardo");
        } else if (curs.getGuanyador() == 4) {           
            System.out.println("El camello ganador es Bonifacio");
        }
    }
}
