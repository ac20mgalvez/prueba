/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Camellos5;


public class Cursa {

    private int[] camellos;
    private int camelloGanador;

    public Cursa() {
        this.camellos = new int[5];
        this.camelloGanador = -1;
    }

    public int getCamells(int num) {
        return camellos[num];
    }
 
    public int getGuanyador() {
        return camelloGanador;  
    }

    public void progreso(int nom, int pasos) {

        camellos[nom] = camellos[nom] + pasos;

        if (camellos[nom] > 59) {
            this.camelloGanador = nom;
        }
    }
}
