/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Camellos5;


public class MainCursa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         
        Cursa cursa = new Cursa();
        
        Camell cam1 = new Camell(1,cursa);
        Camell cam2 = new Camell(2,cursa);
        Camell cam3 = new Camell(3,cursa);
        Camell cam4 = new Camell(4,cursa);
        
        Thread t1 = new Thread(cam1);
        Thread t2 = new Thread(cam2);
        Thread t3 = new Thread(cam3);
        Thread t4 = new Thread(cam4);
        
        Pantalla p1 = new Pantalla(cursa);
        
        p1.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
