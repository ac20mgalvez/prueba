/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Camellos5;

import java.util.Random;


public class Camell implements Runnable {

    private int nom;
    private int pasos;
    private Cursa curs;

    public Camell(int nom, Cursa curs) {
        this.nom = nom;
        this.curs = curs;
    }

    @Override
    public void run() {

        Random rnd = new Random();
        for (int i = 0; i < 60; i++) {
            this.pasos = rnd.nextInt(3) + 1;
            curs.progreso(this.nom, this.pasos);
           
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }
}
